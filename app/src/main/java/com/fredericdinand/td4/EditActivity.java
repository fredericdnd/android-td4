package com.fredericdinand.td4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

import static com.fredericdinand.td4.MainActivity.MyPREFERENCES;

public class EditActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String FIRST_NAME_KEY = "FIRST_NAME_KEY";
    public static final String LAST_NAME_KEY = "LAST_NAME_KEY";
    public static final String CHARACTER_ICON = "CHARACTER_ICON";

    private EditText firstNameInput;
    private EditText lastNameInput;
    private RadioGroup radioGroup;
    private Button saveButton;

    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        firstNameInput = findViewById(R.id.first_name_input);
        lastNameInput = findViewById(R.id.last_name_input);
        radioGroup = findViewById(R.id.radio_group);
        saveButton = findViewById(R.id.save_button);
        saveButton.setOnClickListener(this);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        initRadioButtons();
    }

    private void initRadioButtons() {
        ArrayList<String> array = new ArrayList<>();
        array.add("😈");
        array.add("🤓");
        array.add("😘");
        array.add("🙂");

        for (int i = 0; i < array.size(); i++) {
            RadioButton rb = new RadioButton(this);
            rb.setText(array.get(i));
            radioGroup.addView(rb);

            rb.setChecked(i == 0);
        }
    }

    @Override
    public void onClick(View v) {
        String firstName = firstNameInput.getText().toString();
        String lastName = lastNameInput.getText().toString();

        if (firstName.isEmpty() || lastName.isEmpty()) {
            Toast.makeText(this, "Tous les champs sont requis", Toast.LENGTH_SHORT).show();
            return;
        }

        int id = v.getId();

        if (id == R.id.save_button) {
            System.out.println(firstName);
            System.out.println(lastName);
            RadioButton radioButton = findViewById(radioGroup.getCheckedRadioButtonId());
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(FIRST_NAME_KEY, firstName);
            editor.putString(LAST_NAME_KEY, lastName);
            editor.putString(CHARACTER_ICON, (String) radioButton.getText());
            editor.apply();
            finish();
        }
    }
}