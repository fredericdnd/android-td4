package com.fredericdinand.td4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button editButton = findViewById(R.id.edit_button);
        editButton.setOnClickListener(this);

        Button deleteButton = findViewById(R.id.delete_button);
        deleteButton.setOnClickListener(this);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        Bundle bundle = new Bundle();
        ResultFragment fragment = new ResultFragment();
        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, fragment).commit();
    }

    @Override
    protected void onResume(){
        super.onResume();
        System.out.println(sharedpreferences.getString(EditActivity.LAST_NAME_KEY, null));
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.edit_button) {
            didTapEdit();
        } else if (id == R.id.delete_button) {
            didTapDelete();
        }
    }

    private void didTapEdit() {
        Intent intent = new Intent(MainActivity.this, EditActivity.class);
        startActivity(intent);
    }

    private void didTapDelete() { }
}